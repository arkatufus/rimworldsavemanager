# Discontinued
I'm not maintaining the save editor for the time being, please go to https://bitbucket.org/Redjordan95/rimworldsavemanager for the latest maintained version.

# RimWorld Save Manager #

This is done for educational purposes, so don't expect it to be all so great (or good, for that matter).
But, it does do the basic skill/trait editing, and hopefully without (m)any issues.
Copy the executable file (.exe) into your game directory and run it.

#### Disclaimer: Use at your own risk, I am not responsible for your loss of progress. ####

### ChangeLog ###
#### v0.54.3 ####
* Major code base rewrite and refactor.
* Changes are made in place on the save file.
* Non-destructive file write, file are not completely rebuild when written to disk, only change deltas are modified.
#### v0.54.2 ####
* Fixed background story ID generation bug, background story can be saved properly now.
* Background story combo box pulldown entries are now sorted for less searching headache.
* Fixed background story tooltip so it is less annoying.
#### v0.54.1 ####
* Updated to support Alpha 16
* Added default core backstories
#### v0.54 ####
* Added extra information to backstories (Trait bonuses and disabled skills)
#### v0.53 ####
* Fixed a bug in loading modded traits (Duplicates)
* Added default load/save path to platforms other than Windows
#### v0.52 ####
* Fixed another bug in backstory collection (Please post logs)
* Added the ability to modify biological age
#### v0.51 ####
* Fixed bug in backstory collection
#### v0.5 ####
* Added backstory editing (experimental)
#### v0.42 ####
* Fixed culture dependence in type conversion
#### v0.41 ####
* Fixed some loading bugs
* Added the ability to remove injuries and infections
#### v0.4 ####
* Updated to support Alpha 14
#### v0.3 ####
* Added initial support for injuries and infections
* Updated to support Alpha 13
#### v0.2 ####
* Added support for traits
#### v0.1 ####
* Basic editing of skills and passions